import { useEffect, useState } from 'react';

const useSse = (url: string) => {
  const [data, setNewData] = useState();
  useEffect(() => {
    const sse = new EventSource(url);
    function getRealtimeData(newData: any) {
      setNewData(newData);
    }
    sse.onmessage = (e) => getRealtimeData(JSON.parse(e.data));
    sse.onerror = () => {
      sse.close();
    };
    return () => {
      sse.close();
    };
  }, []);

  return data;
};

export default useSse;
