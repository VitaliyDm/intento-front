import { useEffect, useState } from 'react';

const useFetch = (url: string, options: RequestInit = {}) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(url, options);
        const json = await res.json();
        setResponse(json);
      } catch (catchError) {
        setError(catchError);
      }

      setLoading(false);
    };
    fetchData();
    setLoading(true);
  }, []);

  return { response, error, loading };
};

export default useFetch;
