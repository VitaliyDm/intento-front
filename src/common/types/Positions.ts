enum Align {
  Left = 'left',
  Center = 'center',
  Right = 'right',
  Inherit = 'inherit',
  Justify = 'justify',
}

export default Align;
