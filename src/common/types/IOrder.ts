enum OrderStatus {
  inProgress = 'inProgress',
  done = 'done',
  failed = 'failed',
}

interface IOrder {
  id: number;
  name: string;
  status: OrderStatus;
}

export default IOrder;
export {
  OrderStatus,
};
