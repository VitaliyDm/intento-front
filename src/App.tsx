import React, { useEffect, useMemo, useState } from 'react';

import {
  Button, Box, Container, TextField, CircularProgress, ThemeProvider, createTheme,
} from '@material-ui/core';
import PureTable from './components/Table/PureTable';

import { HOST } from './common/env';

import IOrder from './common/types/IOrder';
import Align from './common/types/Positions';

import useSse from './hooks/useSse';
import useFetch from './hooks/useFetch';
import Modal from './components/Modal/Modal';

interface ISseData { order: IOrder }

const dataColumns = [{
  align: Align.Center,
  text: 'Operation',
}, {
  align: Align.Center,
  text: 'Status',
}];

const getRowsFromData = (data: IOrder) => ({
  cells: [{
    align: Align.Center,
    text: data.name,
  }, {
    align: Align.Center,
    text: data.status,
  }],
  key: data.id.toString(),
});

const theme = createTheme({
  spacing: 6,
  palette: {
    background: {
      paper: '#f5f5f5',
    },
  },
});

function App() {
  const [modalOpened, setModalOpened] = useState(false);
  const [data, setData] = useState([] as IOrder[]);
  const [operationName, setOperationName] = useState('');

  const handleSseData = (newData: { order: IOrder }) => {
    const itemIndex = data.findIndex((item) => item.id === newData.order.id);

    if (itemIndex > -1) {
      setData(
        (oldData) => [
          ...oldData.slice(0, itemIndex), newData.order, ...oldData.slice(itemIndex + 1),
        ],
      );
      return;
    }

    setData((oldData) => [...oldData, newData.order]);
  };

  const sseData = useSse(`${HOST}/order/events`);
  const { response, error, loading } = useFetch(`${HOST}/order`);

  useEffect(() => {
    if (response) {
      // @ts-ignore
      setData(response);
    }
  }, [response]);

  useEffect(() => {
    if (sseData) {
      // @ts-ignore
      handleSseData(sseData as ISseData);
    }
  }, [sseData]);

  const renderTable = useMemo(
    () => {
      if (loading) {
        return <CircularProgress />;
      }

      if (error) {
        return <p>Error on Loading</p>;
      }

      return <PureTable rows={data.map(getRowsFromData)} columns={dataColumns} />;
    },
    [data, error],
  );

  const openModal = () => {
    setModalOpened(true);
  };

  const closeModal = () => {
    setModalOpened(false);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOperationName(event.target.value);
  };

  const handleSubmit = () => {
    fetch(`${HOST}/order`, {
      method: 'POST',
      body: JSON.stringify({ name: operationName }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.statusCode === 500) {
          return;
        }
        handleSseData({ order: (res as IOrder) });
      });

    closeModal();
  };

  return (
    <Container fixed>
      <Box
        display="flex"
        flexDirection="row-reverse"
        bgcolor="background.paper"
      >
        <Box>
          <Button onClick={openModal}>Create</Button>
        </Box>
      </Box>
      {renderTable}
      <ThemeProvider theme={theme}>
        <Modal open={modalOpened} onClose={closeModal} title="Create Operation" onSubmit={handleSubmit}>
          <TextField id="outlined-basic" label="Name of operation" variant="outlined" onChange={handleChange} />
        </Modal>
      </ThemeProvider>
    </Container>
  );
}

export default App;
