import React from 'react';
import {
  Backdrop,
  Container,
  createStyles,
  Fade,
  makeStyles,
  Theme,
  Modal as ModalBase, Button,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';

type IModalProps = {
  open: boolean,
  onClose: any,
  children: React.ReactNode,
  title?: string,
  onSubmit: any,
};

const useStyles = makeStyles((theme: Theme) => createStyles({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    borderRadius: 10,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    padding: theme.spacing(2, 4, 3),
  },
  modalFooter: {
    display: 'flex',
    justifyContent: 'flex-end',
    gap: 10,
  },
  modalTop: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  content: {
    padding: '20px 10px',
  },
}));

const Modal = ({
  open, onClose, children, title, onSubmit,
}: IModalProps) => {
  const classes = useStyles();

  return (
    <ModalBase
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Container>
          <div className={classes.paper}>
            <div className={classes.modalTop}>
              <h2>{title}</h2>
              <Button onClick={onClose}>
                <Close />
              </Button>
            </div>
            <div className={classes.content}>
              {children}
            </div>
            <div className={classes.modalFooter}>
              <Button variant="outlined" onClick={onClose}>
                Cancel
              </Button>
              <Button variant="contained" color="primary" onClick={onSubmit}>
                Create
              </Button>
            </div>
          </div>
        </Container>
      </Fade>
    </ModalBase>
  );
};

Modal.defaultProps = {
  title: '',
};

export default Modal;
