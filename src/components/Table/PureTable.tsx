import React, { useMemo } from 'react';

import {
  TableBody,
  TableHead,
  TableContainer,
  Table,
  TableRow,
  TableCell,
  Paper,
  makeStyles,
} from '@material-ui/core';

import Align from '../../common/types/Positions';

interface ITableCell {
  align: Align;
  text: string;
}

interface ITableRow {
  cells: ITableCell[];
  key: string;
}

type PureTableProps = {
  columns: ITableCell[];
  rows: ITableRow[];
  ariaLabel?: string;
};

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const renderCell = ({ align = Align.Center, text = '' }: ITableCell, index: number) => (
  <TableCell align={align} key={`${text}_${index}`}>{text}</TableCell>
);

const renderBodyRow = (row: ITableRow) => (
  <TableRow key={row.key}>{row.cells.map(renderCell)}</TableRow>
);

const PureTable: React.FC<PureTableProps> = ({
  columns, rows, ariaLabel,
}: PureTableProps) => {
  const classes = useStyles();

  const renderHead = useMemo(
    () => (
      <TableHead>
        <TableRow>{columns.map(renderCell)}</TableRow>
      </TableHead>
    ),
    [columns],
  );

  const renderBody = useMemo(
    () => <TableBody>{rows.map(renderBodyRow)}</TableBody>,
    [rows],
  );

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label={ariaLabel}>
        {renderHead}
        {renderBody}
      </Table>
    </TableContainer>
  );
};

PureTable.defaultProps = {
  ariaLabel: '',
};

export default PureTable;
